//
//  donwloadImageFFI
// 	image
//
//  Created by Darwin Salcedo Soluciones2 on 21/07/16.
//  Copyright © 2015 Darwin Salcedo. All rights reserved.
//
package com.donwload.ds;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.konylabs.android.KonyMain;



public class image {

	static String  URL_IMAGES = "https://fbcdn-photos-d-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-0/s526x395/13709995_597817897067829_3570063804048874689_n.jpg?oh=b17197463c479cb7d68f0a0057bdc0ae&oe=57F3135F&__gda__=1478914488_df1802a06622f53b8e8f7e9ac7671190";//"http://darwone.esy.es/ORIENTAL-700x480.jpg";
	static String  NOMBRE_IMAGES = "DStestImage";
	 static  String NOMBRE_DIRECTORIO = "ASBANC";

	     static ProgressDialog mProgressDialog;
	     static Bitmap test;
	     static Context ctx ;
	     
	     
	     public static void saveImage(String URL,String nombre,String DIR){
	    	 
	    	 ctx = KonyMain.getActivityContext();
	    	 URL_IMAGES = URL;
	    	 NOMBRE_IMAGES = nombre;
	    	 NOMBRE_DIRECTORIO = DIR;
	    	 Log.d(ctx.getClass().toString(),"llegue a saveImage "+URL+" "+nombre);
	    	 new DownloadImage().execute(URL_IMAGES);
	    	 
	     }
	     
	  // DownloadImage AsyncTask
	     public static class DownloadImage extends AsyncTask<String, Void, Bitmap> {
		 
				@Override
				public void onPreExecute() {
					super.onPreExecute();
					Log.d(ctx.getClass().toString(),"llegue a onPreExecute ");
					// Create a progressdialog
					mProgressDialog = new ProgressDialog(ctx);
					// Set progressdialog title
					mProgressDialog.setTitle("Descargando imagen ");
					// Set progressdialog message
					mProgressDialog.setMessage("Cargando...");
					mProgressDialog.setIndeterminate(false);
					// Show progressdialog
					mProgressDialog.show();
				}
		 
				@Override
				public Bitmap doInBackground(String... URL) {
					Log.d(ctx.getClass().toString(),"llegue a doInBackground");
					String imageURL = URL[0];
		 
					Bitmap bitmap = null;
					try {
						// Download Image from URL
						InputStream input = new java.net.URL(imageURL).openStream();
						// Decode Bitmap
						bitmap = BitmapFactory.decodeStream(input);
					} catch (Exception e) {
						e.printStackTrace();
					}
					 test = bitmap;
					return bitmap;
				}
		 
				@Override
				public void onPostExecute(Bitmap result) {
					Log.d(ctx.getClass().toString(),"llegue a onPostExecute");
					// Close progressdialog
					mProgressDialog.dismiss();
					if(result != null){
						String resultado =	guardarImagen(ctx,NOMBRE_IMAGES,result);
						Toast.makeText(ctx," "+ resultado, Toast.LENGTH_LONG).show();
						refreshAndroidGallery(resultado);						
					}else{
						Toast.makeText(ctx," Error en la descarga", Toast.LENGTH_LONG).show();
					}
					
				}
			}
			
	     public static void galleryAddPic(String mCurrentPhotoPath ) {
	    	    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    	    File f = new File(mCurrentPhotoPath);
	    	    Uri contentUri = Uri.fromFile(f);
	    	    mediaScanIntent.setData(contentUri);
	    	    ctx.sendBroadcast(mediaScanIntent);
	    	}

	     public static void refreshAndroidGallery(String path) {
	         if (Build.VERSION.SDK_INT >= 19) {
	        	 galleryAddPic(path);
	         } else {
	        	 ctx.sendBroadcast(new Intent(
	                     Intent.ACTION_MEDIA_MOUNTED,
	                     Uri.parse("file://" + Environment.getExternalStorageDirectory())));
	         }
	     }
	     
			public static String guardarImagen (Context context, String nombre, Bitmap imagen){
				//ContextWrapper cw = new ContextWrapper(context);
				//File dirImages = //cw.getDir(Imagenes, Context.MODE_WORLD_WRITEABLE);
				File myPath = null;
				try {
				Log.d(ctx.getClass().toString(),"llegue a guardarImagen");
					myPath = crearFichero(nombre + ".JPEG");
					
						
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} //new File(dirImages, nombre + ".png");
				
				FileOutputStream fos = null;
				try{
					fos = new FileOutputStream(myPath);
					//force compression
					imagen.compress(Bitmap.CompressFormat.JPEG, 100, fos);
					fos.flush();
				}catch (FileNotFoundException ex){
					ex.printStackTrace();
				}catch (IOException ex){
					ex.printStackTrace();
				}
				return myPath.getAbsolutePath();
			}
			
			public static  File crearFichero(String nombreFichero) throws IOException {
				File ruta = getRuta();
				Log.d(ctx.getClass().toString(),"llegue a crearFichero " + nombreFichero + " ruta "+ruta);
				File fichero = null;
				if (ruta != null){
					fichero = new File(ruta, nombreFichero);
				}
				/**else{
					fichero = new File(actividad.getFilesDir(), NOMBRE_DOCUMENTO);
				}*/
				return fichero;
			}

			/**
			 * Obtenemos la ruta donde vamos a almacenar el fichero.
			 * 
			 * @return
			 */
			public static  File getRuta() {
				Log.d(ctx.getClass().toString(),"llegue a getRuta NOMBRE_DIRECTORIO "+NOMBRE_DIRECTORIO );
				// El fichero sera almacenado en un directorio dentro del directorio
				// Descargas
				File ruta = null;
				if (Environment.MEDIA_MOUNTED.equals(Environment
						.getExternalStorageState())) {
					ruta = new File(
							Environment
									.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
							NOMBRE_DIRECTORIO);
							
					if (ruta != null) {
						if (!ruta.mkdirs()) {
							if (!ruta.exists()) {
								
								return null;
							}
						}
					}
				}
				return ruta;
			}

	
	 
}